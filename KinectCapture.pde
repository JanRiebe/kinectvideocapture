import KinectPV2.*;

KinectPV2 kinect;

boolean doSave=false;
String savePathAndName="C:\\users\\jan\\KinectCaptureOutput\\";
int frameIndex=0;
int firstRecordedFrame=-10000;

PImage colorImage;

void setup(){
  size(512, 424, P3D);
  frameRate(50);
  
  kinect = new KinectPV2(this);
  kinect.enableDepthImg(true);
  kinect.enableColorImg(true);
  
  kinect.init();
}


void draw(){
 
 
 background(0);
 
 colorImage=kinect.getColorImage();
 
 String typeName="";
 
 if(frameIndex%2==0){
   image(colorImage, 512, 0);
   typeName="color";
 }
 else{
   image(kinect.getDepthImage(), 512, 0);
   typeName="depth";
 }
  
  //Saving
  if(doSave){
    save(savePathAndName+typeName+(frameIndex-firstRecordedFrame));
    if(firstRecordedFrame==-10000)
      firstRecordedFrame=frameIndex;
  }   
  
 
  frameIndex++;
  
}


void keyPressed(){
  
  if (key == 'r')
      doSave=!doSave;
  
}